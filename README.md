# Client Management Challenge

Code ini dibuat untuk tujuan technical test di printerous

- Menggunakan Framework Laravel 9 (PHP 8.1)
- Database MySql / MariaDB
- CSS Bootstrap 5

## Installation

```bash
git clone https://gitlab.com/mathin.mochammad/code-test-printerous
cd code-test-printerous
composer install
```
## Copy .env-example ke .env

```bash
cp .env.example .env
```
## Jalankan Migrate dan Seeder

```bash
php artisan migrate:fresh --seed
```

Note: 
User Admin :

admin@admin.com

password : password
