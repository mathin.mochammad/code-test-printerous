<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\PersonController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route Group with middlware auth
Route::group(['middleware' => 'auth'], function () {

    Route::get('/', [HomeController::class, 'index']);
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::group(['prefix' => 'organization'], function () {
        Route::get('/', [OrganizationController::class, 'index'])->name('organization.index');
        Route::get('/{organization}/person', [PersonController::class, 'index'])->name('organization.person.index');
    });

    Route::get('/user', [UserController::class, 'index'])
        ->middleware('is-admin')
        ->name('user.index');
    Route::get('/user/search', [UserController::class, 'search'])->name('user.search');
});
