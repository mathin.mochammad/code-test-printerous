<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'phone',
        'email',
        'avatar',
    ];

    public function organization()
    {
        return $this->hasOneThrough(
            Organization::class,
            OrganizationPerson::class,
            'person_id',
            'id',
            'id',
            'organization_id'
        );
    }

    public function organizationPeople()
    {
        return $this->hasMany(OrganizationPerson::class);
    }
}
