<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'phone',
        'email',
        'website',
        'logo',
        'account_manager_id',
    ];

    public function organizationPeople()
    {
        return $this->hasMany(OrganizationPerson::class);
    }

    public function people()
    {
        return $this->belongsToMany(Person::class);
    }

    public function accountManager()
    {
        return $this->belongsTo(User::class, 'account_manager_id');
    }
}
