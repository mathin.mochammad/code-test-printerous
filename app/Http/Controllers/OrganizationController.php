<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    // index
    public function index()
    {
        return view('organization.index');
    }
}
