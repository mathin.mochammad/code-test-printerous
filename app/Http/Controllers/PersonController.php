<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PersonController extends Controller
{
    // index
    public function index($id)
    {
        return view('person.index', [
            'organization_id' => $id
        ]);
    }
}
