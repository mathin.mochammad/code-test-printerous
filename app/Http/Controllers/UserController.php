<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    function index()
    {
        return view('user.index');
    }

    /** Search User for Select2 */

    public function search(Request $request)
    {
        $search = $request->get('search');

        $users = User::where('name', 'LIKE', '%' . $search . '%')->limit(10)
            ->orderBy('name', 'asc')
            ->get();

        $data = [];

        foreach ($users as $user) {
            $data[] = [
                'id' => $user->id,
                'text' => $user->name,
            ];
        }

        return response()->json([
            'items' => $data,
        ]);
    }
}
