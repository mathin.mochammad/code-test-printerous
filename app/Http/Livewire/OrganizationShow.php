<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use App\Models\Organization;
use App\Models\User;

class OrganizationShow extends Component
{
    use WithPagination, WithFileUploads;

    public $search = '';
    public $name, $email, $phone, $website, $logo, $account_manager_id, $organization_id;

    // pagination theme
    public $paginationTheme = 'bootstrap';

    /** Reset Input Method */
    public function resetInput()
    {
        $this->name = '';
        $this->email = '';
        $this->phone = '';
        $this->website = '';
        $this->logo = '';
        $this->account_manager_id = '';
    }

    /** Close Modal Method */
    public function closeModal()
    {
        $this->resetInput();
    }

    /** Validation Rules */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|min:6',
        ];

        if ($this->website) {
            $rules['website'] = 'required|url';
        }

        if ($this->logo && !is_string($this->logo)) {
            $rules['logo'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }

        if ($this->account_manager_id) {
            $rules['account_manager_id'] = 'exists:users,id';
        }

        return $rules;
    }

    /** Store Method */
    public function store()
    {
        $inputValidated = $this->validate();

        // handle upload
        if ($this->logo && !is_string($this->logo)) {
            $this->logo->store('organizations', 'public');
            $inputValidated['logo'] = $this->logo->hashName();
        }

        Organization::create($inputValidated);

        // send flash session
        session()->flash('message', 'Organization has been created successfully!');
        // reset input
        $this->resetInput();
        // dispatch browser event
        $this->dispatchBrowserEvent('close-modal');
        // redirect to index
    }

    /** Updated Method */
    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    /** Edit Method */
    public function edit(int $id)
    {
        $organization = Organization::with('accountManager')->find($id);

        if ($organization) {
            $this->organization_id = $organization->id;
            $this->name = $organization->name;
            $this->email = $organization->email;
            $this->phone = $organization->phone;
            $this->website = $organization->website;
            $this->account_manager_id = $organization->account_manager_id;
            $this->account_manager = $organization->accountManager;
            $this->logo = asset('/storage/organizations/' . $organization->logo);
        } else {
            // redirect with session
            return redirect()->route('organization.index')->with('error', 'Organization not found!');
        }
    }

    /** Update Method */
    public function update()
    {
        $inputValidated = $this->validate();

        $organization = Organization::find($this->organization_id);

        if ($organization) {

            if ($this->logo && !is_string($this->logo)) {
                $this->logo->store('organizations', 'public');
                $inputValidated['logo'] = $this->logo->hashName();
            }

            $organization->update($inputValidated);

            // send flash session
            session()->flash('success', 'Organization has been updated successfully!');
            // reset input
            $this->resetInput();
            // dispatch browser event
            $this->dispatchBrowserEvent('close-modal');
        } else {
            // redirect with session
            return redirect()->route('organization.index')->with('error', 'Organization not found!');
        }
    }

    /** DeleteId Method */
    public function deleteId(int $id)
    {
        $this->organization_id = $id;
    }

    /** Destroy Method */
    public function destroy()
    {
        $organization = Organization::find($this->organization_id);

        if ($organization) {
            $organization->delete();
            // send flash session
            session()->flash('success', 'Organization has been deleted successfully!');
            // dispatch browser event
            $this->dispatchBrowserEvent('close-modal');
        } else {
            // redirect with session
            return redirect()->route('organization.index')->with('error', 'Organization not found!');
        }
    }

    public function render()
    {
        $organizations = Organization::where('name', 'like', '%' . $this->search . '%')
            ->with('accountManager')
            ->paginate(10);

        $account_managers = User::orderBy('name', 'asc')
            ->get();

        return view('livewire.organization-show', [
            'organizations' => $organizations,
            'account_managers' => $account_managers,
        ]);
    }
}
