<?php

namespace App\Http\Livewire;

use App\Models\Person;
use Livewire\Component;
use App\Models\Organization;
use Livewire\WithPagination;
use Livewire\WithFileUploads;

class PersonShow extends Component
{
    use WithPagination, WithFileUploads;

    public $search = '';
    public $name, $email, $phone, $avatar, $person_id;
    public $organization_id;

    // pagination theme
    public $paginationTheme = 'bootstrap';

    /** Reset Input Method */
    public function resetInput()
    {
        $this->name = '';
        $this->email = '';
        $this->phone = '';
        $this->avatar = '';
    }

    /** Close Modal Method */
    public function closeModal()
    {
        $this->resetInput();
    }

    /** Validation Rules */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|min:6',
        ];

        if ($this->avatar && !is_string($this->avatar)) {
            $rules['avatar'] = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }

        return $rules;
    }

    /** Store Method */
    public function store()
    {
        if (!$this->isAccountManager()) {
            session()->flash('error', 'You are not authorized to edit this person!');
            return;
        }

        $inputValidated = $this->validate();

        // handle upload
        if ($this->avatar && !is_string($this->avatar)) {
            $this->avatar->store('person', 'public');
            $inputValidated['avatar'] = $this->avatar->hashName();
        }

        $person = Person::create($inputValidated);

        $person->organizationPeople()->create([
            'organization_id' => $this->organization_id,
        ]);

        // send flash session
        session()->flash('success', 'Person has been created successfully!');
        // reset input
        $this->resetInput();
        // dispatch browser event
        $this->dispatchBrowserEvent('close-modal');
        // redirect to index
    }

    /** Updated Method */
    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    /** Edit Method */
    public function edit(int $id)
    {
        if (!$this->isAccountManager()) {
            session()->flash('error', 'You are not authorized to edit this person!');
            return;
        }

        $person = Person::find($id);

        if ($person) {
            $this->person_id = $person->id;
            $this->name = $person->name;
            $this->email = $person->email;
            $this->phone = $person->phone;
            $this->website = $person->website;

            if (!is_null($person->avatar)) {
                $this->avatar = asset('/storage/person/' . $person->avatar);
            }
        } else {
            session()->flash('error', 'Person not found!');
        }
    }

    /** Update Method */
    public function update()
    {
        if (!$this->isAccountManager()) {
            session()->flash('error', 'You are not authorized to edit this person!');
            return;
        }

        $inputValidated = $this->validate();

        $person = Person::find($this->person_id);

        if ($person) {

            if ($this->avatar && !is_string($this->avatar)) {
                $this->avatar->store('person', 'public');
                $inputValidated['avatar'] = $this->avatar->hashName();
            }

            $person->update($inputValidated);

            session()->flash('success', 'Person has been updated successfully!');
            $this->resetInput();
        } else {
            session()->flash('error', 'Person Not Found!');
        }

        $this->dispatchBrowserEvent('close-modal');
    }

    /** DeleteId Method */
    public function deleteId(int $id)
    {
        if (!$this->isAccountManager()) {
            session()->flash('error', 'You are not authorized to edit this person!');
            return;
        }

        $this->person_id = $id;
    }

    /** Destroy Method */
    public function destroy()
    {
        if (!$this->isAccountManager()) {
            session()->flash('error', 'You are not authorized to edit this person!');
            return;
        }

        $person = Person::find($this->person_id);

        if ($person) {
            $person->delete();
            // send flash session
            session()->flash('success', 'Person has been deleted successfully!');
            // dispatch browser event
            $this->dispatchBrowserEvent('close-modal');
        } else {
            // redirect with session
            return redirect()->route('organization.person.index')->with('error', 'Person not found!');
        }
    }

    /** Check Is Account Manager */
    public function isAccountManager()
    {
        $organization = Organization::find($this->organization_id);
        return auth()->id() === $organization->account_manager_id;
    }

    /** Mount */
    public function mount($organization_id)
    {
        $this->organization_id = $organization_id;
    }

    public function render()
    {
        $person = Person::where('name', 'like', '%' . $this->search . '%')
            ->whereHas('organization', function ($query) {
                $query->where('organization_id', $this->organization_id);
            })
            ->orderBy('id', 'desc')
            ->paginate(10);

        $organization = Organization::find($this->organization_id);

        return view('livewire.person-show', [
            'person' => $person,
            'organization' => $organization,
        ]);
    }
}
