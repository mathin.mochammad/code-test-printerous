<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\User;

class UserShow extends Component
{
    use WithPagination;

    public $search = '';
    public $name, $email, $role, $password;

    // pagination theme
    public $paginationTheme = 'bootstrap';

    /** Reset Input Method */
    public function resetInput()
    {
        $this->name = '';
        $this->email = '';
        $this->password = '';
        $this->role = '';
    }

    /** Close Modal Method */
    public function closeModal()
    {
        $this->resetInput();
    }

    /** Validation Rules */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'role' => 'required|in:admin,user',
        ];

        return $rules;
    }

    /** Store Method */
    public function store()
    {
        $this->validate();

        User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => bcrypt($this->password),
            'role' => $this->role,
        ]);

        $this->resetInput();
        session()->flash('success', 'User created successfully.');

        $this->dispatchBrowserEvent('close-modal');
    }

    /** Updated Method */
    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    /** Edit Method */
    public function edit($id)
    {
        $user = User::find($id);

        if ($user) {
            $this->user_id = $user->id;
            $this->name = $user->name;
            $this->email = $user->email;
            $this->password = '';
            $this->role = $user->role;
        } else {
            session()->flash('error', 'User not found.');
        }
    }

    /** Update Method */
    public function update()
    {
        $validated = $this->validate();

        if ($validated['password']) {
            $validated['password'] = bcrypt($validated['password']);
        } else {
            unset($validated['password']);
        }

        $user = User::find($this->user_id);

        if ($user) {
            $user->update($validated);
            $this->resetInput();
            session()->flash('success', 'User updated successfully.');
        } else {
            session()->flash('error', 'User not found.');
        }

        $this->dispatchBrowserEvent('close-modal');
    }

    /** Delete Method */
    public function deleteId($id)
    {
        $this->user_id = $id;
    }

    /** Destroy Method */
    public function destroy()
    {
        $user = User::find($this->user_id);

        if ($user) {
            if (!is_null($user->organization)) {
                session()->flash('error', 'User is assigned to an organization. Please remove the user from the organization first.');
            } else {
                $user->delete();
                session()->flash('success', 'User deleted successfully.');
            }
        } else {
            session()->flash('error', 'User not found.');
        }

        $this->dispatchBrowserEvent('close-modal');
    }

    public function render()
    {
        $users = User::where('name', 'like', '%' . $this->search . '%')
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('livewire.user-show', [
            'users' => $users
        ]);
    }
}
