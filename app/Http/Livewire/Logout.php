<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Logout extends Component
{
    // logout method
    public function logout()
    {
        auth()->logout();
        session()->flash('message', 'You are logged out!');
        return redirect('/login');
    }

    public function render()
    {
        return view('livewire.logout');
    }
}
