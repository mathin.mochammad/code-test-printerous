<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Organization;

class IsAccountManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $organization_id = $request->route('organization');

        $organization = Organization::find($organization_id);

        if ($organization->account_manager_id !== auth()->id()) {
            //redirect with error
            abort(403, 'Unauthorized action.');
        }

        return $next($request);
    }
}
