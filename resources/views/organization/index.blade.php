@extends('layouts.app')

@section('content')
<div>
    <livewire:organization-show />
</div>
@endsection

@push('script')
<script>

    window.addEventListener('close-modal', function () {
        $("#create-organization-modal").modal('hide');
        $("#update-organization-modal").modal('hide');
        $("#delete-organization-modal").modal('hide');
    });

</script>

@endpush
