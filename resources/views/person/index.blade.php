@extends('layouts.app')

@section('content')
    <div>
        <livewire:person-show :organization_id="$organization_id" />
    </div>
@endsection

@push('script')
<script>
    window.addEventListener('close-modal', function () {
        $("#create-person-modal").modal('hide');
        $("#update-person-modal").modal('hide');
        $("#delete-person-modal").modal('hide');
    });
</script>

@endpush
