{{-- Create Modal --}}
<div wire:ignore.self class="modal fade" id="create-person-modal" tabindex="-1" aria-labelledby="person-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="person-modal-label">Create Person</h5>
                <button type="button" wire:click="closeModal" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button />
            </div>
            {{-- Multipart formdata --}}
            <form wire:submit.prevent="insert" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="mb-2">
                        <label for="name">Person Name</label>
                        <input type="text" wire:model="name" class="form-control" placeholder="Person Name" />

                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="phone">Phone</label>
                        <input type="text" wire:model="phone" class="form-control" placeholder="Phone" />

                        @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="email">Email</label>
                        <input type="email" wire:model="email" class="form-control" placeholder="Email" />

                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="avatar">Avatar</label>
                        <input type="file" wire:model="avatar" class="form-control" accept="image/png, image/jpeg" />

                        @error('avatar')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    {{-- image preview --}}
                    @if ($avatar)
                    <div class="">
                        <img wire:model="avatar" class="img-thumbnail" width="100" height="100" src="{{ is_string($avatar) ? $avatar : $avatar->temporaryUrl() }}" alt="">
                    </div>
                    @endif
                </div>
            </form>
            <div class="modal-footer">
                <button wire:click="closeModal" type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button wire:click="store" type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

{{-- Update Modal --}}
<div wire:ignore.self class="modal fade" id="update-person-modal" tabindex="-1" aria-labelledby="person-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="person-modal-label">Update Person</h5>
                <button type="button" wire:click="closeModal" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button />
            </div>
            {{-- Modal Fields : name, phone, email, website, logo --}}
            {{-- Multipart formdata --}}
            <form wire:submit.prevent="update" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="mb-2">
                        <label for="name">Person Name</label>
                        <input type="text" wire:model="name" class="form-control" placeholder="Person Name" />

                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="phone">Phone</label>
                        <input type="text" wire:model="phone" class="form-control" placeholder="Phone" />

                        @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="email">Email</label>
                        <input type="email" wire:model="email" class="form-control" placeholder="Email" />

                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="avatar">Avatar</label>
                        <input type="file" wire:model="avatar" class="form-control" accept="image/png, image/jpeg" />

                        @error('avatar')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    {{-- image preview --}}
                    @if ($avatar)
                    <div class="">
                        <img wire:model="avatar" class="img-thumbnail" width="100" height="100" src="{{ is_string($avatar) ? $avatar : $avatar->temporaryUrl() }}" alt="">
                    </div>
                    @endif
                </div>
            </form>
            <div class="modal-footer">
                <button wire:click="closeModal" type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button wire:click="update" type="button" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>

{{-- Delete Modal --}}

<div wire:ignore.self class="modal modal-alert fade" tabindex="-1" id="delete-person-modal" aria-labelledby="person-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-3 shadow">
            <div class="modal-body p-4 text-center">
                <h5 class="mb-0">Are you sure?</h5>
                <p class="mb-0">This record will be deleted.</p>
            </div>
            <div class="modal-footer flex-nowrap p-0">
                <button wire:click="closeModal" type="button" class="btn btn-lg btn-link fs-6 text-decoration-none col-6 m-0 rounded-0" data-bs-dismiss="modal">No thanks</button>
                <button wire:click="destroy" type="button" class="btn btn-lg btn-link fs-6 text-decoration-none col-6 m-0 rounded-0 border-end"><strong>Yes, delete it !</strong></button>
            </div>
        </div>
    </div>
</div>
