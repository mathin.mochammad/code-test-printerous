{{-- Create Modal --}}
<div wire:ignore.self class="modal fade" id="create-user-modal" tabindex="-1" aria-labelledby="user-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="user-modal-label">Add New User</h5>
                <button type="button" wire:click="closeModal" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button />
            </div>
            <form wire:submit.prevent="insert">
                <div class="modal-body">
                    <div class="mb-2">
                        <label for="name">User Name *</label>
                        <input type="text" wire:model="name" class="form-control" placeholder="User Name" />

                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="email">Email *</label>
                        <input type="email" wire:model="email" class="form-control" placeholder="User Email" />

                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="role">Role</label>
                        <select wire:model="role" class="form-control">
                            <option value="">Select Role</option>
                            <option value="admin">Admin</option>
                            <option value="user">User</option>
                        </select>

                        @error('role')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="password">Password *</label>
                        <input type="password" wire:model="password" class="form-control" placeholder="User Password" />

                        @error('password')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button wire:click="closeModal" type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button wire:click="store" type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

{{-- Update Modal --}}
<div wire:ignore.self class="modal fade" id="update-user-modal" tabindex="-1" aria-labelledby="user-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="user-modal-label">Update Organization</h5>
                <button type="button" wire:click="closeModal" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button />
            </div>
            {{-- Modal Fields : name, phone, email, website, logo --}}
            {{-- Multipart formdata --}}
            <form wire:submit.prevent="insert" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="mb-2">
                        <label for="name">User Name *</label>
                        <input type="text" wire:model="name" class="form-control" placeholder="User Name" />

                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="email">Email *</label>
                        <input type="email" wire:model="email" class="form-control" placeholder="User Email" />

                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="role">Role</label>
                        <select wire:model="role" class="form-control">
                            <option value="">Select Role</option>
                            <option value="admin">Admin</option>
                            <option value="user">User</option>
                        </select>

                        @error('role')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="password">Password *</label>
                        <input type="password" wire:model="password" class="form-control" placeholder="User Password" />

                        @error('password')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button wire:click="closeModal" type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button wire:click="update" type="button" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>

{{-- Delete Modal --}}

<div wire:ignore.self class="modal modal-alert fade" tabindex="-1" id="delete-user-modal" aria-labelledby="user-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-3 shadow">
            <div class="modal-body p-4 text-center">
                <h5 class="mb-0">Are you sure?</h5>
                <p class="mb-0">This record will be deleted.</p>
            </div>
            <div class="modal-footer flex-nowrap p-0">
                <button wire:click="closeModal" type="button" class="btn btn-lg btn-link fs-6 text-decoration-none col-6 m-0 rounded-0" data-bs-dismiss="modal">No thanks</button>
                <button wire:click="destroy" type="button" class="btn btn-lg btn-link fs-6 text-decoration-none col-6 m-0 rounded-0 border-end"><strong>Yes, delete it !</strong></button>
            </div>
        </div>
    </div>
</div>
