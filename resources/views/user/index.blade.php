@extends('layouts.app')

@section('content')
    <div>
        <livewire:user-show />
    </div>
@endsection

@push('script')
<script>

    window.addEventListener('close-modal', function () {
        $("#create-user-modal").modal('hide');
        $("#update-user-modal").modal('hide');
        $("#delete-user-modal").modal('hide');
    });
</script>

@endpush
