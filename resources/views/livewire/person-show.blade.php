<div>
    @include('person.modal')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{-- If Session Exists --}}
                @if (session()->has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        {{ session()->get('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session()->get('error') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                {{-- Bootstrap Card --}}
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">{{ $organization->name }} PIC</h5> <br>
                        <input type="search" wire:model="search" class="form-control float-end mx-2" placeholder="Search..." style="width: 230px" />
                        @if ($organization->account_manager_id == auth()->id())
                        <button class="btn btn-primary float-end" wire:click="resetInput" data-bs-toggle="modal" data-bs-target="#create-person-modal">Add New PIC</button>
                        @endif
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    @if ($organization->account_manager_id == auth()->id())
                                    <th>Actions</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                {{-- Forelse Data --}}
                                @forelse ($person as $item)
                                <tr>
                                    <td>
                                        {{-- If Logo Exists --}}
                                        @if ($item->avatar)
                                            @if (substr($item->avatar, 0, 4) === "http")
                                            <img src="{{ $item->avatar }}" width="50" height="50">
                                            @else
                                            <img src="{{ asset('storage/person/' . $item->avatar) }}" width="50" height="50">
                                            @endif
                                        @else
                                            No avatar
                                        @endif
                                    </td>
                                    <td class="align-middle">{{ $item->name }}</td>
                                    <td class="align-middle">{{ $item->phone }}</td>
                                    <td class="align-middle">{{ $item->email }}</td>
                                    @if ($organization->account_manager_id == auth()->id())
                                    <td class="text-nowrap align-middle" width="100">
                                        <button wire:click="edit({{ $item->id }})" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#update-person-modal">Edit</button>
                                        <button wire:click="deleteId({{ $item->id }})" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#delete-person-modal">Delete</button>
                                    </td>
                                    @endif
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6" class="text-center">No Data</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{-- Paginate Data --}}
                        <div>
                            {{ $person->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
