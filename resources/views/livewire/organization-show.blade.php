<div>
    @if (auth()->user()->isAdmin())
        @include('organization.modal')
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{-- If Session Exists --}}
                @if (session()->has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        {{ session()->get('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session()->get('error') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                {{-- Bootstrap Card --}}
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Organizations</h5>
                        <input type="search" wire:model="search" class="form-control float-end mx-2" placeholder="Search..." style="width: 230px" />
                        <button class="btn btn-primary float-end" wire:click="resetInput" data-bs-toggle="modal" data-bs-target="#create-organization-modal">Add New Organization</button>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Logo</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Website</th>
                                    <th>Account Manager</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- Forelse Data --}}
                                @forelse ($organizations as $item)
                                <tr>
                                    <td>
                                        {{-- If Logo Exists --}}
                                        @if ($item->logo)
                                            @if (substr($item->logo, 0, 4) === "http")
                                            <img src="{{ $item->logo }}" width="50" height="50">
                                            @else
                                            <img src="{{ asset('storage/organizations/' . $item->logo) }}" width="50" height="50">
                                            @endif
                                        @else
                                            No Logo
                                        @endif
                                    </td>
                                    <td class="align-middle">{{ $item->name }}</td>
                                    <td class="align-middle">{{ $item->phone }}</td>
                                    <td class="align-middle">{{ $item->email }}</td>
                                    <td class="align-middle">{{ $item->website }}</td>
                                    <td class="align-middle">{{ $item->accountManager?->name }}</td>
                                    <td class="text-nowrap align-middle">
                                        @if (auth()->user()?->isAdmin())
                                        <button wire:click="edit({{ $item->id }})" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#update-organization-modal">Edit</button>
                                        <button wire:click="deleteId({{ $item->id }})" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#delete-organization-modal">Delete</button>
                                        @endif
                                        <a href="{{ route('organization.person.index', $item->id) }}" class="btn btn-sm btn-success" role="button">{{ $item->account_manager_id == auth()->user()->id ? 'Manage PIC' : 'Show PIC' }}</a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6" class="text-center">No Data</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{-- Paginate Data --}}
                        <div>
                            {{ $organizations->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
