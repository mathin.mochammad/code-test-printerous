<div>
    @include('user.modal')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{-- If Session Exists --}}
                @if (session()->has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        {{ session()->get('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session()->get('error') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                {{-- Bootstrap Card --}}
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">User</h5>
                        <input type="search" wire:model="search" class="form-control float-end mx-2" placeholder="Search..." style="width: 230px" />
                        <button class="btn btn-primary float-end" wire:click="resetInput" data-bs-toggle="modal" data-bs-target="#create-user-modal">Add New User</button>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- Forelse Data --}}
                                @forelse ($users as $item)
                                <tr>
                                    <td class="align-middle">{{ $item->name }}</td>
                                    <td class="align-middle">{{ $item->email }}</td>
                                    <td class="align-middle">{{ $item->role }}</td>
                                    <td class="text-nowrap align-middle" width="50">
                                        <button wire:click="edit({{ $item->id }})" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#update-user-modal">Edit</button>
                                        @if ($item->id != 1)
                                        <button wire:click="deleteId({{ $item->id }})" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#delete-user-modal">Delete</button>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6" class="text-center">No Data</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{-- Paginate Data --}}
                        <div>
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
