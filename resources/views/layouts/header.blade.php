<main>
    <header class="p-3 mb-3 border-bottom">
        <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-dark text-decoration-none">
                    <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
                </a>

                <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                    <li><a href="/" class="nav-link px-2 link-secondary">Dashboard</a></li>
                    @if (auth()->check())
                    <li><a href="{{ route('organization.index') }}" class="nav-link px-2 link-dark">Organization</a></li>
                    @endif
                    @if (auth()->user()?->isAdmin())
                    <li><a href="{{ route('user.index') }}" class="nav-link px-2 link-dark">Users</a></li>
                    @endif
                </ul>

                @if (auth()->check())
                <div class="dropdown text-end">
                    <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="https://ui-avatars.com/api/?name={{ auth()->user()->name }}" alt="mdo" width="32" height="32" class="rounded-circle">
                        {{-- User name Login --}}
                        <span class="ml-2 d-none d-lg-inline-block">{{ auth()->user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1" style="">
                        <li><hr class="dropdown-divider"></li>
                        {{-- SignOut Link Livewire --}}
                        <li><livewire:logout /></li>
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </header>
</main>
